FROM manycoding/robotframework:selenium_2.53.6

RUN apk add --update --no-cache \
    bash \
    wget \
    curl \
    git \
    xvfb \
    dbus \
    firefox-esr \
    ttf-freefont

RUN pip install -U \
    requests

ADD xvfb-run /usr/bin/
RUN chmod +x /usr/bin/xvfb-run

RUN mkdir -p /robot/results
WORKDIR /robot
VOLUME /robot/results results
ADD login.robot login.robot
CMD xvfb-run -a robot --outputdir results login.robot
