*** Settings ***
Library  Selenium2Library  5  screenshot_root_directory=results/sreenshots
Suite Setup  Setup Suite
Suite Teardown  Close All Browsers

*** Test Cases ***
Login Should Not Work
  Wait Until Element Is Visible  //*[@class="ePanelLinks_Label"]
  Mouse Over  //*[@class="ePanelLinks_Label"]
  Wait Until Element Is Visible  //*[contains(@class, "ePanelLinks_term") and contains(., "Вход")]
  Click Element  //*[contains(@class, "ePanelLinks_term") and contains(., "Вход")]
  Select Frame  //div[contains(@class, "bPopupHint") and not(contains(@style,"display: none;"))]//*[@class="eUserLoginMenu_frame jsLoginFrame"]
  Wait Until Element Is Visible  Login
  Input Text  Login  Login
  Input Password  Password  Password
  Click Element  //*[@id='Authentication']/../../div
  Wait Until Element Is Visible  //*[. = "Проверьте правильность ввода логина и пароля."]

Login Should Work
  Input Text  Login  ozoninterview@mfsa.ru
  Input Password  Password  MuchTesting1
  Click Element  //*[@id='Authentication']/../../div
  Unselect Frame
  Wait Until Element Is Visible  //*[@class="ePanelLinks_Label" and . = "Ozon"]

*** Keywords ***
Setup Suite
  Create Webdriver    Firefox
  Go To    https://www.ozon.ru/
